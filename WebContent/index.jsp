<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Cargue Perezoso</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/file-input.css" rel="stylesheet">
</head>
<body background="img/B86.jpg">
	<%
	String mensaje = (String) request.getAttribute("mensaje");
	if (mensaje != null) {%>
	<div class="alert alert-danger">
	  <label>Error: <%=mensaje%></label> 
	</div>
	<%} %>
	<div>
		<div align="center">
			<h1 style="color: #fff; font-family: monospace;"></h1>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-3"></div>
		<div class="col-lg-4 col-md-4 col-sm-6 jumbotron grad1" align="center"
			style="margin-top:10%; border-radius: 10px 10px 10px 10px; -moz-border-radius: 10px 10px 10px 10px; -webkit-border-radius: 10px 10px 10px 10px; border: 0px solid #000000;">
			<form action="CarguePerezoso" enctype="multipart/form-data"
				method="post">
				<div align="center">
					<div class="form-group">
						<label style="color: #fff">Cédula:</label> <input
							id="cedulaParticipante" name="cedulaParticipante" type="text"
							required="true" maxlength="11" style="width: 25%;">
					</div>
					<div class="form-group">
						<input type="file" name="archivoEntrada" id="archivoEntrada"
							class="inputfile inputfile-1" required="true" accept="text/plain"/> <label
							for="archivoEntrada"> <svg
								xmlns="http://www.w3.org/2000/svg" class="iborrainputfile"
								width="20" height="17" viewBox="0 0 20 17"> <path
								d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg>
							<span class="iborrainputfile">Seleccionar archivo</span>
						</label>
					</div>
				</div>
				<input type="submit" class="btn btn-info" value="Descargar Solución" />
			</form>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6"></div>
	</div>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="js/custom-file-input.js"></script>
</body>
</html>