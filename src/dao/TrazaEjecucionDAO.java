/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import conexionBD.Conexion;

public class TrazaEjecucionDAO extends Conexion {

    private PreparedStatement psInsertar;
    public Boolean insertarTrazaEjecucion(String cedulaParticipante) {
        try {
            conectar();
            conexion.createStatement();
            this.psInsertar = conexion.prepareStatement("insert into traza_ejecucion (cedula_participante,fecha_ejecucion)" + "values (?,?)");

            this.psInsertar.setString(1, cedulaParticipante);
            java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
            this.psInsertar.setDate(2, sqlDate);
            this.psInsertar.execute();
            desconectar();
        } catch (SQLException ex) {
            Logger.getLogger(TrazaEjecucionDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TrazaEjecucionDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;

    }

}


