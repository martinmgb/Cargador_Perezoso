package conexionBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

	protected  Connection conexion;
	private final String usuario="root";
	private final String password="";
	private final String servidor = "jdbc:mysql://localhost/cargue_perezoso";
	private final String driver="com.mysql.jdbc.Driver";

	public void conectar() throws SQLException, ClassNotFoundException{
		Class.forName(driver);
		conexion=DriverManager.getConnection(servidor, usuario, password);
		System.out.println("conexion exitosa");
	}

	public void desconectar() throws SQLException{
		conexion.close();
		System.out.println("Desconexion Exitosa");

	}

}
