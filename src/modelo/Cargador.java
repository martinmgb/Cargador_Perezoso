package modelo;

import java.util.ArrayList;

public class Cargador {
	
	private Bolsa bolsa;
	private Integer numeroViajes; 
	private Integer diasTrabajo; 

	public Cargador(Integer diasTrabajo) {
		super();
		this.bolsa = new Bolsa();
		this.numeroViajes = 0;
		this.diasTrabajo = diasTrabajo; 
	}
	
	/**
	 * @param elementosDelDia 
	 * @return el arreglo de elementos del dia ordenados de mayor a menor
	 */
	public ArrayList<Double> ordenarElementosDelDia(ArrayList<Double> elementosDelDia) {
		ArrayList<Double> auxiliar = new ArrayList<Double>();
		for(Double elemento : elementosDelDia){
			if(auxiliar.size()!=0){
				ubicarPosicionDelElemento(auxiliar, elemento);
			}else{
				auxiliar.add(elemento);
			}
		}
		return auxiliar;
	}
	
	/**
	 * @param auxiliar arreglo que esta siendo usado para ordenar los elementos
	 * @param elemento elemento al que se le buscara posicion
	 */
	public void ubicarPosicionDelElemento(ArrayList<Double> auxiliar, Double elemento) {
		for(int i=0; i<auxiliar.size();i++){
			//Ordenar de mayor a menor peso los elementos
			if(elemento.compareTo(auxiliar.get(i))>=0 ){
				auxiliar.add(i, elemento);
				break;
			}else if(auxiliar.size()==i+1){
				auxiliar.add(elemento);
			}
		}
	}
	
	/**
	 * @param elementosDelDia elementos que aun no se han llevado al camion
	 * @param pesoMinimo peso minimo a criterio del supervisor
	 */
	public void llenarBolsa(ArrayList<Double> elementosDelDia, Double pesoMinimo) {
		Double elementoMayorPeso = elementosDelDia.remove(0); //Elemento de mayor peso
		int i=1;
		//Calculamos el peso que puede pensar el supervisor (peso del elemento superior y la cantidad de elementos)
		//para saber la cantidad de elementos agregar con el elemento de mayor peso en la parte superior de la bolsa
		while(elementoMayorPeso*i < pesoMinimo && i<elementosDelDia.size()){
			i++;
		}
		//Llenamos la bolsa con elementos de menor peso hasta que quede un elemento por agregar
		while(i>1){
			this.bolsa.agregarElemento(elementosDelDia.remove(elementosDelDia.size()-1));
			i--;
		}
		//Verificamos si para la proxima bolsa podemos lograr que no reprendan al cargador por poseer menos del peso minimo
		//sino colocamos el resto de los elementos en la bolsa actual
		if(elementosDelDia.get(0)*elementosDelDia.size()<pesoMinimo){
			while(elementosDelDia.size()>0){
				this.bolsa.agregarElemento(elementosDelDia.remove(elementosDelDia.size()-1));
			}
		}
		//Agregamos el elemento de mayor peso para hacer pensar al supervisor 
		//de llevar por lo menos el minimo de libras
		this.bolsa.agregarElemento(elementoMayorPeso);
	}
	
	public void llevarBolsaAlCamion() {
		this.bolsa.vaciar();
		this.numeroViajes++;
	}

	public void nuevoDia() {
		this.bolsa.vaciar();
		numeroViajes = 0;
	}
	
	public void getCantidadElementosEnLaBolsa() {
		this.bolsa.getCantidadElementos();
	}
	
	public Bolsa getBolsa() {
		return bolsa;
	}

	public Integer getNumeroViajes() {
		return numeroViajes;
	}

	public Integer getDiasTrabajo() {
		return diasTrabajo;
	}

	public void setDiasTrabajo(Integer diasTrabajo) {
		this.diasTrabajo = diasTrabajo;
	}
	
	
}
