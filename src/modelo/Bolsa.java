package modelo;

import java.util.Stack;

public class Bolsa {
	
	private Stack<Double> elementos; 
	
	public Bolsa() {
		super();
		this.elementos = new Stack<Double>();
	}

	public Integer getCantidadElementos() {
		return this.elementos.size();
	}
	
	public Double getPesoElementoSuperior() {
		return this.elementos.lastElement();
	}
	
	public void agregarElemento(Double elemento) {
		this.elementos.push(elemento);
	}
	
	public void vaciar() {
		this.elementos.clear();;
	}

	public Stack<Double> getElementos() {
		return elementos;
	}

	public void setElementos(Stack<Double> elementos) {
		this.elementos = elementos;
	}
	
	
	
	
}
