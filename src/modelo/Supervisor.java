package modelo;

public class Supervisor {
	private static final Double PESO_MINIMO = 50.0;
	
	/**Metodo usado solo para asegurar que el supervisor no reprendera al cargador
	 * @param cantidadElementos
	 * @param pesoElementoSuperior
	 * @return true si es valido o false si no es valido el peso estimado de la bolsa
	 */
	public Boolean pesoEstimadoValido(Integer cantidadElementos, Double pesoElementoSuperior){
		if(cantidadElementos*pesoElementoSuperior >= PESO_MINIMO){
			return true;
		}else{
			return false;
		}
	}

	public Double getPesoMinimo() {
		return PESO_MINIMO;
	}
	
	
}
