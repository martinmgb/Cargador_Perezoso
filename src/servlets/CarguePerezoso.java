package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import dao.TrazaEjecucionDAO;
import modelo.Cargador;
import modelo.Supervisor;

/**
 * Servlet implementation class CarguePerezoso
 */
@WebServlet("/CarguePerezoso")
@MultipartConfig
public class CarguePerezoso extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private BufferedReader in;
	private static final String MENSAJE_ERROR_DIAS_TRABAJO="Días de trabajo fuera de limite, por favor revisar el archivo de entrada.";
	private static final String MENSAJE_ERROR_CANTIDAD_ELEMENTOS="Cantidad de elementos fuera de limite, por favor revisar el archivo de entrada.";
	private static final String MENSAJE_ERROR_PESO_ELEMETO="Peso de elemento fuera de limite, por favor revisar el archivo de entrada.";
	private static final String MENSAJE_ERROR_FORMATO_NUMERO="Formato de número no válido.";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CarguePerezoso() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String cadena = "";
		int numeroElementosDelDia;
		Double elemento;
		
		ArrayList<Double> elementosDelDia = new ArrayList<Double>();
		try {
			Part filePart = request.getPart("archivoEntrada"); 
			this.in = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
			
			Cargador wilson = new Cargador(Integer.valueOf(this.in.readLine()));
			if(!validarDiasDeTrabajo(wilson.getDiasTrabajo())){
				request.setAttribute("mensaje", CarguePerezoso.MENSAJE_ERROR_DIAS_TRABAJO);
		        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			}
			Supervisor julie = new Supervisor();
			
			for(int i=1; i<=wilson.getDiasTrabajo();i++){
				elementosDelDia.clear();
				wilson.nuevoDia();
				numeroElementosDelDia=Integer.valueOf(this.in.readLine());//cantidad de elementos del dia i
				
				if(!validarCantidadElementosDia(numeroElementosDelDia)){
					request.setAttribute("mensaje", CarguePerezoso.MENSAJE_ERROR_CANTIDAD_ELEMENTOS);
			        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
				}
				//leemos los elementos correspondientes al dia i
				for(int j=0;j<numeroElementosDelDia;j++){
					elemento=Double.valueOf(this.in.readLine());
					if(!validarPesoElemento(elemento)){
						request.setAttribute("mensaje", CarguePerezoso.MENSAJE_ERROR_PESO_ELEMETO);
				        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
					}
					elementosDelDia.add(elemento);
				}
				//Operacion del dia de trabajo con los elementos del dia, cargador, y supervisor asignado
				trabajoDelDia(elementosDelDia, wilson, julie);
				
				//Escritura en el archivo txt la linea de la solucion del dia i
				if(i!=1){
					cadena+="\nCase #"+i+": "+wilson.getNumeroViajes();
				}else{
					cadena+="Case #"+i+": "+wilson.getNumeroViajes();
				}
			}
			//Creacion y salida del archivo de descarga
			response.setHeader("Content-Disposition", "attachment; filename=solucion.txt");
			OutputStream outStream = response.getOutputStream();
			outStream.write(cadena.getBytes());
			outStream.flush();
			outStream.close();

		}catch(NumberFormatException nfe){
			request.setAttribute("mensaje", CarguePerezoso.MENSAJE_ERROR_FORMATO_NUMERO);
			getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		}finally{
			//Guardar en BD la traza de ejecucion
			guardarTrazaEjecucion(request.getParameter("cedulaParticipante"));
		}
	}
	
	/**Metodo que se encarga de hacer las operaciones de un dia de trabajo
	 * @param elementosDelDia 
	 * @param cargador
	 * @param supervisor
	 */
	protected void trabajoDelDia(ArrayList<Double> elementosDelDia, Cargador cargador, Supervisor supervisor) {
		// TODO Auto-generated method stub
		elementosDelDia = cargador.ordenarElementosDelDia(elementosDelDia);//Ordenar elementos de mayor a menor peso
		while(elementosDelDia.size()>0){
			cargador.llenarBolsa(elementosDelDia, supervisor.getPesoMinimo());//Llenar bolsa dependiendo del criterio del supervisor
			//El supervisor estima el peso para ver si tiene que reprender al cargador
			if(supervisor.pesoEstimadoValido(cargador.getBolsa().getCantidadElementos(),
					cargador.getBolsa().getPesoElementoSuperior())){
				cargador.llevarBolsaAlCamion();
			}else{
				//Mensaje de Julie reprendiendo a Wilson (Este caso no debe ocurrir)
				System.out.println("#############MENSAJE DE ERROR############");
			}
		}
	}
	
	/** Metodo que guarda la traza de ejecucion en BD
	 * @param cedulaParticipante
	 */
	protected void guardarTrazaEjecucion(String cedulaParticipante){
		TrazaEjecucionDAO daoTrazaEjecucion = new TrazaEjecucionDAO();
		daoTrazaEjecucion.insertarTrazaEjecucion(cedulaParticipante);
	}
	
	protected Boolean validarDiasDeTrabajo(Integer diasTrabajo){
		if(diasTrabajo>=1 && diasTrabajo<=500){
			return true;
		}else{
			return false;
		}
	}
	
	protected Boolean validarCantidadElementosDia(Integer cantidadElementosDia){
		if(cantidadElementosDia>=1 && cantidadElementosDia<=100){
			return true;
		}else{
			return false;
		}
	}
	
	protected Boolean validarPesoElemento(Double peso){
		if(peso>=1 && peso<=100){
			return true;
		}else{
			return false;
		}
	}
	
	


}
