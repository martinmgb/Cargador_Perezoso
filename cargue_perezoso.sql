-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 21-02-2017 a las 16:32:04
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cargue_perezoso`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traza_ejecucion`
--

CREATE TABLE `traza_ejecucion` (
  `cedula_participante` text NOT NULL,
  `fecha_ejecucion` date NOT NULL,
  `id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `traza_ejecucion`
--

INSERT INTO `traza_ejecucion` (`cedula_participante`, `fecha_ejecucion`, `id`) VALUES
('', '2017-02-20', 1),
('23542438', '2017-02-20', 2),
('22225558', '2017-02-20', 3),
('22225558', '2017-02-20', 4),
('23542438', '2017-02-20', 5),
('235', '2017-02-20', 6),
('235', '2017-02-20', 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `traza_ejecucion`
--
ALTER TABLE `traza_ejecucion`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `traza_ejecucion`
--
ALTER TABLE `traza_ejecucion`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
